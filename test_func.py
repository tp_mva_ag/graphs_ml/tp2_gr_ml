from Face_recognition_with_HFS.Face_reco_func import *

# two_moons_hfs(False, 10)
# print(hard_vs_soft_hfs(False, 0.5, var=100, c_l=100, c_u=1, k=5))
# offline_face_recognition(False, "Gaussian", large_dataset=True)

images, labels = augmented_image_loading(False)

print('Filter = box')
images_box = face_preprocessing(images, 'box', False)
lab_box = face_labeling(images_box, labels, var=100000, k=10)

print('Filter = Gaussian')
images_gau = face_preprocessing(images, 'Gaussian', False)
lab_gau = face_labeling(images_gau, labels, var=100000, k=10)

print('Filter = bilinear')
images_bil = face_preprocessing(images, 'bilinear', False)
lab_bil = face_labeling(images_bil, labels, var=100000, k=10)
