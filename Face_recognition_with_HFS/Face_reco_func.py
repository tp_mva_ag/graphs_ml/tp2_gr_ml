import matplotlib.pyplot as pyplot
import scipy.misc as sm
import cv2
import os
from tqdm import tqdm

from harmonic_function_solution.HFS_func import *


def offline_face_recognition(plot_the_dataset=False, chosen_filter="Gaussian", var=100000, k=10, large_dataset=False):
    """ The total processing face dataset

    :param plot_the_dataset: Boolean
    :param chosen_filter: Chosen filter: Gaussian, bilinear or box
    :param var:
    :param k:
    :param large_dataset: Choose the large dataset
    :return:
    """

    # We load the dataset
    print('Filter = ', chosen_filter)
    if large_dataset:
        images, labels = augmented_image_loading(plot_the_dataset)
    else:
        images, labels = face_image_loading(plot_the_dataset)

    # We do the preprocessing on the dataset
    print('Pre-processing')
    images = face_preprocessing(images, chosen_filter, plot_the_dataset)

    # We determine the labels
    rlabels = face_labeling(images, labels, var, k)

    return rlabels


def face_image_loading(plot_the_dataset=False):
    """ Load the dataset of images, without preprocessing

    :param plot_the_dataset:
    :return: images: The images dataset
    :return: labels: The corresponding labels
    """

    # Parameters
    cc = cv2.CascadeClassifier('./data/haarcascade_frontalface_default.xml')
    frame_size = 96
    # Loading images
    images = np.zeros((100, frame_size ** 2))
    labels = np.zeros(100)

    # Loading dataset
    for i in np.arange(10):
        for j in np.arange(10):
            im = sm.imread("./data/10faces/%d/%02d.jpg" % (i, j + 1))
            box = cc.detectMultiScale(im)
            top_face = {"area": 0}

            for cfx, cfy, clx, cly in box:
                face_area = clx * cly
                if face_area > top_face["area"]:
                    top_face["area"] = face_area
                    top_face["box"] = [cfx, cfy, clx, cly]

            fx, fy, lx, ly = top_face["box"]
            gray_im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
            gray_face = gray_im[fy:fy + ly, fx:fx + lx]

            # resize the face and reshape it to a row vector, record labels
            gf_resized = cv2.resize(gray_face, (frame_size, frame_size))
            images[j * 10 + i] = np.reshape(gf_resized, -1)
            labels[j * 10 + i] = i + 1

    # Plotting the dataset
    if plot_the_dataset:
        pyplot.figure(1)
        for i in range(100):
            pyplot.subplot(10, 10, i + 1)
            pyplot.axis('off')
            pyplot.imshow(images[i].reshape(frame_size, frame_size))
            r = '{:d}'.format(i + 1)
            if i < 10:
                pyplot.title('Person ' + r)
        pyplot.show()

    return images, labels


def augmented_image_loading(plot_the_dataset=False):
    """ Load the AUGMENTED dataset of images, without preprocessing

    :param plot_the_dataset:
    :return: images: The images dataset
    :return: labels: The corresponding labels
    """

    # Parameters
    cc = cv2.CascadeClassifier('./data/haarcascade_frontalface_default.xml')
    frame_size = 96
    nbimgs = 50
    # Loading images
    images = np.zeros((10 * nbimgs, frame_size ** 2))
    labels = np.zeros(10 * nbimgs)

    # for i in np.arange(10):
    for i in tqdm(np.arange(10), desc="Loading images"):
        imgdir = "./extended_dataset/%d" % i
        imgfns = os.listdir(imgdir)
        for j, imgfn in enumerate(np.random.choice(imgfns, size=nbimgs)):
            im = sm.imread("{}/{}".format(imgdir, imgfn))
            box = cc.detectMultiScale(im)
            top_face = {"area": 0, "box": (0, 0, *im.shape[:2])}

            for cfx, cfy, clx, cly in box:
                face_area = clx * cly
                if face_area > top_face["area"]:
                    top_face["area"] = face_area
                    top_face["box"] = [cfx, cfy, clx, cly]

            fx, fy, lx, ly = top_face["box"]
            gray_im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
            gray_face = gray_im[fy:fy + ly, fx:fx + lx]

            # resize the face and reshape it to a row vector, record labels
            gf_resized = cv2.resize(gray_face, (frame_size, frame_size))
            images[j * 10 + i] = np.reshape(gf_resized, -1)
            labels[j * 10 + i] = i + 1

    if plot_the_dataset:

        pyplot.figure(1)
        for i in range(10 * nbimgs):
            pyplot.subplot(nbimgs, 10, i + 1)
            pyplot.axis('off')
            pyplot.imshow(images[i].reshape(frame_size, frame_size))
            r = '{:d}'.format(i + 1)
            if i < 10:
                pyplot.title('Person ' + r)
        pyplot.show()

    return images, labels


def face_preprocessing(images, chosen_filter="Gaussian", plot_the_dataset=False):
    """ Apply the preprocessing on the images of the dataset

    :param images: The list of images
    :param chosen_filter: The chosen filter: Gaussian, bilinear or box
    :param plot_the_dataset:
    :return: images: The filtered dataset
    """

    frame_size = 96

    # for i in range(np.size(images, 0)):
    for i in tqdm(range(np.size(images, 0)), desc="Preprocessing {}".format(chosen_filter)):
        gray_face = (images[i].reshape(frame_size, frame_size)).astype(np.uint8)

        #######################################################################
        # Apply preprocessing to balance the image (color/lightning), such    #
        # as filtering (cv.boxFilter, cv.GaussianBlur, cv.bilinearFilter) and #
        # equalization (cv.equalizeHist).                                     #
        #######################################################################

        # Applying choosen filter
        if chosen_filter == "box":
            gray_filtered = cv2.boxFilter(gray_face, -1, (5, 5))
        elif chosen_filter == "bilinear":
            gray_filtered = cv2.bilateralFilter(gray_face, 9, 75, 75)
        elif chosen_filter == "Gaussian":
            gray_filtered = cv2.GaussianBlur(gray_face, (5, 5), 0)
        else:
            gray_filtered = gray_face

        gf = cv2.equalizeHist(gray_filtered)

        # resize the face and reshape it to a row vector, record labels
        images[i] = np.reshape(gf, -1)

    # Plotting the dataset
    if plot_the_dataset:
        pyplot.figure(1)
        for i in range(100):
            pyplot.subplot(10, 10, i + 1)
            pyplot.axis('off')
            pyplot.imshow(images[i].reshape(frame_size, frame_size))
            r = '{:d}'.format(i + 1)
            if i < 10:
                pyplot.title('Person ' + r)
        pyplot.show()

    return images


def face_labeling(images, labels, var=100000, k=10):
    """ Select 4 random labels per person, and then try to guess all the other labels

    :param images: The dataset, preprocessed
    :param labels: All the known labels
    :param var: The variance for building the graph
    :param k: Parameter for the graph
    :return: labels:
    """

    nb_masked = 4
    nb_samples = np.size(labels, 0)
    y_masked = np.zeros(nb_samples)

    lab_idx = []

    # Selecting 4 random labels per person
    while np.size(lab_idx) < nb_masked:
        idx = int(np.random.uniform(0, nb_samples/10))
        if idx not in lab_idx:
            y_masked[idx * 10:(idx + 1) * 10] = labels[idx * 10:(idx + 1) * 10]
            lab_idx.append(idx)

    # Compute the hard HFS estimation
    rlabels = hard_hfs(images, y_masked, var=var, k=k)

    # Plots #
    pyplot.subplot(121)
    pyplot.imshow(labels.reshape((int(nb_samples/10), 10)))

    pyplot.subplot(122)
    pyplot.imshow(rlabels.reshape((int(nb_samples/10), 10)))
    pyplot.title("Acc: {}".format(np.equal(rlabels, labels).mean()))

    pyplot.show()

    return rlabels
