from helper import *
import scipy.io as sio


def build_laplacian_regularized(x, laplacian_regularization, var=1.0, eps=0.0, k=0, laplacian_normalization=""):
    """ A skeleton function to construct a laplacian from data, needs to be completed

    :param x: (n x m) matrix of m-dimensional samples
    :param laplacian_regularization: regularization to add to the laplacian
    :param var: Variance
    :param eps: Epsilon parameter
    :param k: k parameter for k-NN graph
    :param laplacian_normalization: "sym", "rw"
    :return:
    """

    # build the similarity graph W
    w = build_similarity_graph(x, var, eps, k)

    #################################################################
    # build the laplacian                                           #
    # L: (n x n) dimensional matrix representing                    #
    #    the Laplacian of the graph                                 #
    # Q: (n x n) dimensional matrix representing                    #
    #    the laplacian with regularization                          #
    #################################################################

    # We get the Laplacian with the given function from helper.py
    laplacian = build_laplacian(w, laplacian_normalization)
    # We compute Q = L + regul * Id
    q = laplacian + laplacian_regularization * np.eye(np.size(laplacian, 0))

    return q


def mask_labels(y, nb_unmasked):
    """ A skeleton function to select a subset of labels and mask the rest

    :param y: (n x 1) label vector, where entries Y_i take a value in [1..C] (num classes)
    :param nb_unmasked: number of unmasked (revealed) labels to include in the output
    :return: y_masked: (n x 1) masked label vector, where entries Y_i take a value in [1..C]
                        if the node is labeled, or 0 if the node is unlabeled (masked)
    """

    num_samples = np.size(y, 0)

    #################################################################
    # randomly sample l nodes to remain labeled, mask the others    #
    #################################################################

    y_masked = np.zeros(num_samples)

    # We make sure we have all the labels in our new 'masked' set
    while not np.min([unique in y_masked for unique in np.unique(y)]):
        y_masked = np.zeros(num_samples)
        i = 0
        # The loop for un-masking 'nb_unmasked' labels
        while i < nb_unmasked:
            idx = int(np.random.uniform(0, len(y)))
            y_masked[idx] = y[idx]
            i = np.size(np.nonzero(y_masked))

    return y_masked


def hard_hfs(x, y, laplacian_regularization=1.0, var=1.0, eps=0, k=0, laplacian_normalization=""):
    #  a skeleton function to perform hard (constrained) HFS,
    #  needs to be completed
    #
    #  Input
    #  X:
    #      (n x m) matrix of m-dimensional samples
    #  Y:
    #      (n x 1) vector with nodes labels [1, ... , num_classes] (0 is unlabeled)
    #
    #  Output
    #  labels:
    #      class assignments for each (n) nodes

    num_samples = np.size(x, 0)
    cl = np.unique(y)

    ####################################################################
    # l_idx = (l x num_classes) vector with indices of labeled nodes   #
    # u_idx = (u x num_classes) vector with indices of unlabeled nodes #
    ####################################################################

    l_idx = [idx for idx in range(num_samples) if y[idx]]
    u_idx = [idx for idx in range(num_samples) if not y[idx]]

    #################################################################
    # compute the hfs solution, remember that you can use           #
    #   build_laplacian_regularized and build_similarity_graph      #
    # f_l = (l x num_classes) hfs solution for labeled.             #
    # it is the one-hot encoding of Y for labeled nodes.            #
    # example:                                                      #
    # if cl=[0,3,5] and Y=[0,0,0,3,0,0,0,5,5], then f_l is a 3x2    #
    # binary matrix where the first column codes the class '3'      #
    # and the second the class '5'.                                 #
    # In case of 2 classes, you can also use +-1 labels             #
    # f_u = (u x num_classes) hfs solution for unlabeled            #
    #################################################################

    # The f_l matrix
    f_l = np.transpose(np.array(
        [[y[i] == classe for i in l_idx]
         for classe in cl if classe]))

    # We compute Q
    q = build_laplacian_regularized(x, laplacian_regularization, var, eps,
                                    k=k, laplacian_normalization=laplacian_normalization)

    # We get l_uu and l_ul
    l_uu = q[:, u_idx][u_idx]
    l_ul = q[:, l_idx][u_idx]

    # We compute f_u
    f_u = np.dot(np.linalg.inv(l_uu),
                 -1 * np.dot(l_ul, f_l))

    #################################################################
    # compute the labels assignment from the hfs solution           #
    # labels: (n x 1) class assignments [1,2,...,num_classes]        #
    #################################################################

    labels = y.copy()
    labels[u_idx] = [cl[1 + np.argmax(f_u[i])] for i in range(len(u_idx))]

    #################################################################
    #################################################################
    return labels


def two_moons_hfs(large=False, laplacian_regularization=0, var=1.0):
    """ A function to perform HFS on the two moons dataset

    :param large: boolean if we use the large dataset
    :param laplacian_regularization:
    :param var:
    :return:
    """
    # a skeleton function to perform HFS, needs to be completed

    # load the data
    in_data = sio.loadmat('./data/data_2moons_hfs.mat')
    if large:
        in_data = sio.loadmat('./data/data_2moons_hfs_large.mat')

    x = in_data['X']
    y = in_data['Y']

    k = 5

    #################################################################
    # choose the experiment parameter                               #
    #################################################################

    nb_kept = 4  # number of labeled (unmasked) nodes provided to the hfs algorithm
    #################################################################
    #################################################################

    # mask labels
    y_masked = mask_labels(y, nb_kept)

    #################################################################
    # compute hfs solution using either soft_hfs.m or hard_hfs.m    #
    #################################################################

    labels = hard_hfs(x, y_masked, laplacian_regularization, var=var, k=k)

    #################################################################
    #################################################################

    plot_classification(x, y, labels, var=var, eps=0, k=k)
    accuracy = np.mean(labels == np.squeeze(y))

    return accuracy


def soft_hfs(x, y, c_l, c_u, laplacian_regularization=0.5, var=1.0, eps=0, k=0, laplacian_normalization=""):
    """a skeleton function to perform soft (unconstrained) HFS, needs to be completed

    :param x: (n x m) matrix of m-dimensional samples
    :param y: (n x 1) vector with nodes labels [1, ... , num_classes] (0 is unlabeled)
    :param c_l: coefficient for C matrix
    :param c_u: coefficient for C matrix
    :param laplacian_regularization:
    :param var:
    :param eps:
    :param k:
    :param laplacian_normalization:
    :return: labels: class assignments for each (n) nodes
    """

    num_samples = np.size(x, 0)
    cl = np.unique(y)
    # num_classes = len(cl) - 1

    #################################################################
    # compute the target y for the linear system                       #
    # y = (n x num_classes) target vector                              #                          
    # l_idx = (l x num_classes) vector with indices of labeled nodes   #
    # u_idx = (u x num_classes) vector with indices of unlabeled nodes #
    ####################################################################

    l_idx = [idx for idx in range(num_samples) if y[idx]]
    u_idx = [idx for idx in range(num_samples) if not y[idx]]

    # For each classe (!= 0), we create a column to y_vect, in which the i-th value is :
    #   1 if y[i] == classe,
    #   0 otherwise
    y_vect = np.transpose(np.array(
        [[y[i] == classe for i in range(num_samples)]
         for classe in cl if classe]))

    #################################################################
    #################################################################

    #################################################################
    # compute the hfs solution, remember that you can use           #
    #   build_laplacian_regularized and build_similarity_graph      #
    # f = (n x num_classes) hfs solution                            #
    # C = (n x n) diagonal matrix with c_l for labeled samples      #
    #             and c_u otherwise                                 #
    #################################################################

    # We compute the Q
    q = build_laplacian_regularized(x, laplacian_regularization, var, eps, k, laplacian_normalization)

    # We create the C matrix
    c = np.eye(num_samples)
    for i in range(num_samples):
        c[i][i] = c_l * (i in l_idx) + c_u * (i in u_idx)

    c_inv = np.linalg.inv(c)

    # We compute f
    f = np.dot(
        np.linalg.inv(np.dot(c_inv, q) + np.eye(num_samples)),
        y_vect)

    #################################################################
    # compute the labels assignment from the hfs solution           #
    # label: (n x 1) class assignments [1, ... ,num_classes]        #
    #################################################################

    labels = [cl[1 + np.argmax(f[i])] for i in range(num_samples)]

    return labels


def hard_vs_soft_hfs(large=False, laplacian_regularization=0.5, var=1000.0,  c_l=100, c_u=1, k=5):
    # a skeleton function to confront hard vs soft HFS, needs to be completed

    # load the data
    in_data = sio.loadmat('./data/data_2moons_hfs.mat')
    if large:
        in_data = sio.loadmat('./data/data_2moons_hfs_large.mat')

    x = in_data['X']
    y = in_data['Y']

    # randomly sample 20 labels
    nb_kept_lab = 20
    # mask labels
    y_masked = mask_labels(y, nb_kept_lab)

    # We change the value of 4 labeled samples
    y_masked[y_masked != 0] = label_noise(y_masked[y_masked != 0], 4)

    #################################################################
    # compute hfs solution using soft_hfs.m and hard_hfs.m          #
    # remember to use Y_masked (the vector with some labels hidden  #
    # as input and not Y (the vector with all labels revealed)      #
    #################################################################

    hard_labels = hard_hfs(x, y_masked, laplacian_regularization,
                           var=var, k=k, laplacian_normalization="")
    soft_labels = soft_hfs(x, y_masked, c_l, c_u, laplacian_regularization,
                           var=var, k=k, laplacian_normalization="")

    #################################################################
    #################################################################

    y_masked[y_masked == 0] = np.squeeze(y)[y_masked == 0]

    plot_classification_comparison(x, y, hard_labels, soft_labels, var=var, eps=0, k=k)
    accuracy = [np.mean(hard_labels == np.squeeze(y)), np.mean(soft_labels == np.squeeze(y))]

    return accuracy
